# streamdeck-ts

This is aggressively early in development. It's catered almost exclusively to my use with only vague consideration for extensibility, ease of use, configuration, etc.

You're welcome to poke around, try it, contribute, etc but you've been warned.

## Prerequisites

You'll need the following

- Node - I'm using version 20. Others may work
- `pnpm` - https://pnpm.io/installation

## Usage

1. Clone the repo to your path of choice - we'll call it `ROOT` going forward
1. Create your configs - see individual plugins for examples. They should be in `ROOT/configs`
1. Navigate to `ROOT` and run `pnpm start`
