export const randomColor = (): [number, number, number] => {
	const crand = () => Math.floor(Math.random() * 255);

	return [crand(), crand(), crand()];
};
