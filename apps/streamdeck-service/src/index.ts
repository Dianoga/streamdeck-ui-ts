import path from 'path';
import { StreamDeckService } from './streamdeck';

const shutdownHandlers: Array<() => Promise<void> | void> = [];

let service: StreamDeckService;

const run = async () => {
	const configPath = path.resolve('..', '..', 'configs');

	service = new StreamDeckService({ configPath });
	await service.start();
};

const cleanShutdown = async (signal: string) => {
	console.log(`Received ${signal}: Shutting down nicely`);

	await service.stop();

	process.exit();
};

process.on('SIGINT', cleanShutdown);
process.on('SIGTERM', cleanShutdown);
process.on('SIGHUP', cleanShutdown);

run();
