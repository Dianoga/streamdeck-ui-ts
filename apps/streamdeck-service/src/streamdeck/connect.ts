import {
	listStreamDecks,
	openStreamDeck,
	DeviceModelId,
} from 'shared/streamdeck';
import { randomColor } from '../utils/color';

type ConnectOptions = { path?: string; debug?: boolean };

export async function connect({
	path: devicePath,
	debug = false,
}: ConnectOptions = {}) {
	if (!devicePath) {
		const decks = await listStreamDecks();
		devicePath = decks[0].path;
	}

	const deck = await openStreamDeck(devicePath);

	if (debug) {
		deck.on('down', (control) => {
			console.log('%s %d down', control.type, control.index);

			if (control.type !== 'button' || control.feedbackType !== 'lcd') {
				return;
			}

			deck.fillKeyColor(control.index, ...randomColor());
		});

		// This probably doesn't do anything you want
		// deck.on('up', (control) => {
		// 	console.log('%s %d up', control.type, control.index);
		// 	deck.clearKey(control.index);
		// });

		if (deck.MODEL === DeviceModelId.PLUS) {
			deck.on('rotate', (control, amount) => {
				console.log('Encoder rotate #%d (%d)', control.index, amount);
			});

			deck.on('lcdShortPress', (control, pos) => {
				console.log('lcd short press #%d (%d, %d)', control.id, pos.x, pos.y);
			});

			deck.on('lcdLongPress', (control, pos) => {
				console.log('lcd long press #%d (%d, %d)', control.id, pos.x, pos.y);
			});

			deck.on('lcdSwipe', (control, pos, pos2) => {
				console.log(
					'lcd swipe #%d (%d, %d)->(%d, %d)',
					control.id,
					pos.x,
					pos.y,
					pos2.x,
					pos2.y,
				);
			});
		}
	}

	// Fired whenever an error is detected by the `node-hid` library.
	// Always add a listener for this event! If you don't, errors will be silently dropped.
	deck.on('error', (error) => {
		console.error(error);
	});

	return deck;
}
