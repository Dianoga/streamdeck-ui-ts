import { StreamDeck } from 'shared/streamdeck';
import { connect } from '.';
import type { BaseConfigSchema, PluginBase } from '@plugins/base';
import { glob } from 'glob';
import path from 'path';
import { readFile } from 'fs/promises';
import chokidar, { type FSWatcher } from 'chokidar';

type StreamDeckServiceOptions = {
	configPath: string;
};

type Config = BaseConfigSchema & {
	plugins: { package: string; config: Record<string, any> }[];
};

export class StreamDeckService {
	protected configPath: StreamDeckServiceOptions['configPath'];
	protected deck?: StreamDeck;
	protected shutdownHandlers: (() => Promise<void> | void)[] = [];

	private _serviceStatus: 'stopped' | 'starting' | 'running' | 'stopping' =
		'stopped';
	private configWatcher?: FSWatcher;

	public constructor({ configPath }: StreamDeckServiceOptions) {
		this.configPath = configPath;
	}

	public get serviceStatus() {
		return this._serviceStatus;
	}

	public async start() {
		if (this._serviceStatus !== 'stopped') {
			throw Error('Service is already running');
		}

		this._serviceStatus = 'starting';

		this.deck = await connect({ debug: false });

		this.configWatcher = chokidar.watch(this.configPath, {
			persistent: false,
		});

		this.configWatcher.on('change', (path) => {
			console.log('config change - restarting service', path);
			this.restart();
		});

		const configs = await this.loadConfigs();

		for (const config of configs) {
			if (typeof config.base.buttons === 'number') {
				config.base.buttons = [config.base.buttons];
			}

			if (!Array.isArray(config.base.buttons)) {
				config.base.buttons = [];
			}

			if (typeof config.base.encoders === 'number') {
				config.base.encoders = [config.base.encoders];
			}

			if (!Array.isArray(config.base.encoders)) {
				config.base.encoders = [];
			}

			for (const pc of config.plugins) {
				const pluginConfig = {
					base: config.base,
					enabled: config.enabled ?? true,
					...pc.config,
				};

				if (!pluginConfig.enabled) continue;

				try {
					const Plugin = (await import(pc.package)).default;

					const plugin: PluginBase<typeof pluginConfig> = new Plugin(
						pluginConfig,
					);
					plugin.init(this.deck);

					this.shutdownHandlers.push(plugin.shutdown);
				} catch (e) {
					console.error(e);
				}
			}
		}

		this._serviceStatus = 'running';
	}

	public async stop() {
		this._serviceStatus = 'stopping';

		for (const handler of this.shutdownHandlers) {
			await handler();
		}

		if (this.deck !== undefined) {
			await this.deck.clearPanel();
			await this.deck.close();
			delete this.deck;
		}

		if (this.configWatcher !== undefined) {
			await this.configWatcher.close();
			delete this.configWatcher;
		}

		this._serviceStatus = 'stopped';
	}

	public async restart() {
		await this.stop();
		await this.start();
	}

	private async loadConfigs() {
		const configFiles = await glob('*.json', { cwd: this.configPath });
		const configs: Config[] = [];

		for (let file of configFiles) {
			const raw = await readFile(path.resolve(this.configPath, file), {
				encoding: 'utf-8',
			});

			configs.push({ ...JSON.parse(raw), configFile: file });
		}

		return configs;
	}
}
