import { PluginBase } from '@plugins/base';
import { ConfigSchema } from './config';
import { StreamDeck } from 'shared/streamdeck';
import { format } from 'date-fns';

export default class PluginClock extends PluginBase<ConfigSchema> {
	private drawTimeout: NodeJS.Timeout | undefined;

	public init = (deck: StreamDeck) => {
		this.deck = deck;

		this.run();
	};

	public shutdown = () => {
		clearTimeout(this.drawTimeout);
	};

	private run = async () => {
		try {
			await this.drawClock();
		} catch {
		} finally {
			// Clear the timeout if it somehow exists
			if (this.drawTimeout) clearTimeout(this.drawTimeout);

			const now = new Date();
			const nextRun = 60000 - now.getSeconds() * 1000 - now.getMilliseconds();
			this.drawTimeout = setTimeout(this.run, nextRun);
		}
	};

	protected drawClock = async () => {
		if (!this.deck) return;

		const button = this.config.base.buttons[0];
		const iconSize = this.getButtonSize(button);

		const date = new Date();

		const svg = `
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ${iconSize} ${iconSize}" version="1.1">
			<g font-family="sans-serif" fill="#fff" text-anchor="middle">
				<text font-size="40px" font-weight="700" x="${iconSize / 2}" y="44">
					${format(date, 'HH:mm')}
				</text>
				<text font-size="24px" font-weight="700" x="${iconSize / 2}" y="68">
					${format(date, 'E')}
				</text>
				<text font-size="24px" font-weight="700" x="${iconSize / 2}" y="${iconSize - 15}">
					${format(date, 'MMM d')}
				</text>
			</g>
		</svg>`;
		await this.drawSVGButton(svg, button);
	};
}
