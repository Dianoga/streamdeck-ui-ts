# Clock Plugin

Shows a basic clock with day of week and date

## Example config

```json
{
	"base": { "buttons": 3 },
	"plugins": [
		{
			"package": "@plugins/clock",
			"config": {}
		}
	]
}
```
