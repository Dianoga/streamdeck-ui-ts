# Audio Toggle Plugin

Allows toggling between audio devices using PulseAudio

## Example config

```json
{
	"base": { "buttons": 0 },
	"plugins": [
		{
			"package": "@plugins/audio-toggle",
			"config": {
				"outputDevices": [
					{
						"name": "Speakers",
						"match": "iec958-stereo",
						"icon": "mdi:speaker"
					},
					{
						"name": "Headphones",
						"match": "Arctis.*game",
						"icon": "mdi:headphones"
					}
				]
			}
		}
	]
}
```

| Name                     | Value   | Description               |
| ------------------------ | ------- | ------------------------- |
| `outputDevices`          | Array[] |                           |
| `outputDevices.[].name`  | string  | Not really used right now |
| `outputDevices.[].match` | string  | Regex to match            |
| `outputDevices.[].icon`  | string  | Icon to display           |
