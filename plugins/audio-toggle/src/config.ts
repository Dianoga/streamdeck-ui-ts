import { baseConfigSchema } from '@plugins/base';
import { z } from 'shared';

export const configSchema = baseConfigSchema.merge(
	z.object({
		outputDevices: z
			.object({
				name: z.string().optional(),
				match: z.string(),
				icon: z.string(),
			})
			.array(),
	}),
);

export type ConfigSchema = z.infer<typeof configSchema>;
