import { PluginBase } from '@plugins/base';
import { PulseAudio } from 'pulseaudio.js';
import { ConfigSchema } from './config';
import { StreamDeck, StreamDeckControlDefinition } from 'shared/streamdeck';

export default class PluginAudioToggle extends PluginBase<ConfigSchema> {
	private pa = new PulseAudio();
	private currentSink!: string;

	public init = async (deck: StreamDeck) => {
		this.deck = deck;

		deck.on('up', this.handleButton);

		await this.pa.connect();

		this.updateServerInfo();

		this.pa.on('event.sink.change', this.updateServerInfo);
		this.pa.on('event.server.change', this.updateServerInfo);
	};

	public shutdown = async () => {
		await this.pa.disconnect();

		this.deck?.off('up', this.handleButton);
	};

	private updateServerInfo = async () => {
		const info = await this.pa.getServerInfo();

		// If unchanged, do nothing
		if (this.currentSink === info.defaultSink) return;

		this.currentSink = info.defaultSink;

		const { sink } = this.matchSinkToDevice(this.currentSink);
		const icon = sink?.icon ?? 'mdi:help-box';
		for (const button of this.config.base.buttons) {
			this.drawIcon(icon, button);
		}
	};

	private handleButton = (control: StreamDeckControlDefinition) => {
		if (control.type !== 'button') return;

		if (this.config.base.buttons.includes(control.index)) {
			this.toggleDevice();
		}
	};

	private matchSinkToDevice = (sinkName: string) => {
		const ci = this.config.outputDevices.findIndex((check) => {
			const regex = new RegExp(check.match);
			const match = sinkName.match(regex);
			return match;
		});

		return {
			idx: ci,
			sink: ci > -1 ? this.config.outputDevices[ci] : undefined,
		};
	};

	// @todo update to maintain current option if desired not found
	private toggleDevice = async () => {
		const sinks = await this.pa.getAllSinks();

		// Find current sink
		const { idx: ci } = this.matchSinkToDevice(this.currentSink);

		let desired = this.config.outputDevices[0];
		if (ci < this.config.outputDevices.length - 1) {
			desired = this.config.outputDevices[ci + 1];
		}

		const i = sinks.findIndex((s) => {
			const regex = new RegExp(desired.match);
			const match = (s.name as string).match(regex);
			if (match) return true;
		});

		if (i > -1) {
			console.log(`Switching sink to ${sinks[i].name}`);
			await this.pa.setDefaultSink(sinks[i].name as string);
			this.updateServerInfo();
		} else {
			console.log('Device not found', [...sinks.map((s) => s.name)]);
		}
	};
}
