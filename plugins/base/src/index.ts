import type {
	StreamDeck,
	StreamDeckButtonControlDefinitionLcdFeedback,
} from 'shared/streamdeck';
import { type BaseConfigSchema, baseConfigSchema } from './config';
import sharp from 'sharp';
import { getMdiIcon } from './utils/icons';

export { baseConfigSchema, type BaseConfigSchema };

export class PluginBase<T extends BaseConfigSchema> {
	protected deck?: StreamDeck;
	protected paddingX = 10;
	protected paddingY = 10;

	constructor(protected config: T) {
		if (config) baseConfigSchema.parse(config);
	}

	public init = (deck: StreamDeck): Promise<void> | void => {
		this.deck = deck;
	};

	public shutdown = (): Promise<void> | void => {};

	protected getButtonSize = (buttonIndex: number) => {
		if (!this.deck) throw Error('this.deck not defined');

		const button = this.deck.CONTROLS.find(
			(c) =>
				c.type === 'button' &&
				c.feedbackType === 'lcd' &&
				c.index === buttonIndex,
		) as StreamDeckButtonControlDefinitionLcdFeedback | undefined;

		if (!button) throw Error(`Button index not LCD: ${buttonIndex}`);

		// This assumes square buttons. Should be safe for now but may bite us later
		return button.pixelSize.width;
	};

	protected getLcdSegmentSize = (encoder: number) => {
		if (!this.deck) throw Error('this.deck not defined');

		const lcd = this.deck.CONTROLS.find((c) => c.type == 'lcd-segment');
		if (!lcd) {
			throw Error('No LCD segment discovered');
		}

		const segments = this.deck.CONTROLS.filter(
			(c) => c.type === 'encoder',
		).length;

		return {
			width: lcd.pixelSize.width / segments - this.paddingX,
			height: lcd.pixelSize.height - this.paddingY,
		};
	};

	/**
	 * @todo make this more flexible, filenames, web requests, etc
	 */
	protected drawIcon = async (iconName: string, buttonIndex: number) => {
		if (!this.deck) return;

		const button = this.deck.CONTROLS.find(
			(c) =>
				c.type === 'button' &&
				c.feedbackType === 'lcd' &&
				c.index === buttonIndex,
		) as StreamDeckButtonControlDefinitionLcdFeedback | undefined;

		if (!button) return;

		try {
			const icon = await getMdiIcon(iconName, {
				fillColor: '#fff',
				size: this.getButtonSize(buttonIndex),
				fallback: 'mdi:help-box',
			});

			if (!icon) return;

			await this.deck?.fillKeyBuffer(buttonIndex, icon);
		} catch (e) {
			console.error('Icon error', e);
		}
	};

	protected drawSVGButton = async (svg: string, button: number) => {
		if (!this.deck) return;

		try {
			const img = await sharp(Buffer.from(svg)).flatten().raw().toBuffer();
			await this.deck?.fillKeyBuffer(button, img);
		} catch (e) {
			console.error('drawSvg error', e);
		}
	};

	protected drawSVGLcdControl = async (svg: string, encoder: number) => {
		if (!this.deck) return;

		try {
			const lcd = this.deck.CONTROLS.find((c) => c.type == 'lcd-segment');

			if (!lcd) {
				throw Error('No LCD segment discovered');
			}

			const { width, height } = this.getLcdSegmentSize(encoder);
			const x = encoder * width + this.paddingX / 2;
			const y = this.paddingY / 2;

			const img = await sharp(Buffer.from(svg)).flatten().raw().toBuffer();
			await this.deck?.fillLcdRegion(lcd.id, x, y, img, {
				format: 'rgb',
				width,
				height,
			});
		} catch (e) {
			console.error('drawSvg error', e);
		}
	};
}
