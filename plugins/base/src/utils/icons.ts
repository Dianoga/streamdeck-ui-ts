import sharp from 'sharp';

const camelize = (str: string) => {
	return str
		.replace(/(?:^\w|\[A-Z\]|\b\w)/g, (word, index) => {
			return index === 0 ? word.toLowerCase() : word.toUpperCase();
		})
		.replace(/[\s\:-]+/g, '');
};

export type IconOpts = {
	fallback?: string;
	fillColor: string;
	size: number;
	asSvg?: boolean;
	x?: number;
	y?: number;
};

export function getMdiIcon(
	iconName: string,
	opts: IconOpts & { asSvg: true },
): Promise<string>;
export function getMdiIcon(
	iconName: string,
	opts: IconOpts & { asSvg?: false },
): Promise<Buffer>;
export async function getMdiIcon(
	iconName: string,
	opts: IconOpts,
): Promise<string | Buffer> {
	const { fillColor, size, fallback, asSvg = false, x = 0, y = 0 } = opts;

	const iconJs = camelize(iconName);
	const iconPath = require(`@mdi/js`)[iconJs];

	if (!iconPath) {
		if (fallback && fallback !== iconName) {
			// @ts-expect-error this should work fine but TS complains
			return getMdiIcon(fallback, opts);
		}
		throw Error(`Invalid icon ${iconName} : ${iconJs} : ${iconPath}`);
	}

	const svg = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width='${size}' height='${size}' x="${x}" y="${y}">
			<path fill="${fillColor}" d="${iconPath}"/>
		</svg>`;

	if (asSvg) return svg;

	const img = await sharp(Buffer.from(svg)).flatten().raw().toBuffer();
	return img;
}
