import { z } from 'shared';

export const baseConfigSchema = z.object({
	base: z.object({
		buttons: z.number().array(),
		encoders: z.number().array(),
	}),
	enabled: z.boolean().optional().default(true),
});

export type BaseConfigSchema = z.infer<typeof baseConfigSchema>;
