import { baseConfigSchema } from '@plugins/base';
import { z } from 'shared';

export const configSchema = baseConfigSchema.merge(
	z.object({
		outputDevices: z
			.object({
				name: z.string().optional(),
				match: z.string(),
				sinks: z
					.object({
						match: z.string(),
						icon: z.string().optional(),
					})
					.array(),
			})
			.array(),
	}),
);

export type ConfigSchema = z.infer<typeof configSchema>;
