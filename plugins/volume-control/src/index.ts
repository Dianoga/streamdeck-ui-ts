import { PluginBase } from '@plugins/base';
import { percentToVolume, PulseAudio, volumeToPercent } from 'pulseaudio.js';
import { ConfigSchema } from './config';
import { StreamDeck, StreamDeckControlDefinition } from 'shared/streamdeck';
import { getMdiIcon } from '../../base/src/utils/icons';

type PulseAudioSink = {
	name: string;
	index: number;
	volume: { current: number[] };
};

type SinkControl = {
	index: number;
	name: string;
	volume: number;
	icon?: string;
};

export default class PluginVolumeControl extends PluginBase<ConfigSchema> {
	private pa = new PulseAudio();
	private currentSink?: string;
	private currentSinkControl?: SinkControl;
	private controls: SinkControl[] = [];

	public init = async (deck: StreamDeck) => {
		this.deck = deck;

		deck.on('up', this.handleEncoderPress);
		deck.on('rotate', this.handleEncoderRotate);

		await this.pa.connect();

		this.updateServerInfo();

		this.pa.on('event.sink.change', (...args) => {
			// console.log('event.sink.change', args);
			this.updateServerInfo();
		});
		this.pa.on('event.server.change', (...args) => {
			// console.log('event.server.change', args);
			this.updateServerInfo();
		});
	};

	public shutdown = async () => {
		this.pa.removeAllListeners('event.sink.change');
		this.pa.removeAllListeners('event.server.change');
		await this.pa.disconnect();

		this.deck?.off('up', this.handleEncoderPress);
		this.deck?.off('rotate', this.handleEncoderRotate);
	};

	private handleEncoderPress = (control: StreamDeckControlDefinition) => {
		if (!(control.type === 'encoder') || !this.currentSinkControl) return;

		if (this.config.base.encoders.includes(control.index)) {
			// Only one control, nothing to do
			if (this.controls.length === 1) return;

			const currentIdx = this.controls.findIndex(
				(c) => c === this.currentSinkControl,
			);

			// Current control not found, bail
			if (currentIdx === -1) {
				console.error('Current control not found');
				return;
			}

			let next = currentIdx + 1;
			if (next === this.controls.length) next = 0;
			this.currentSinkControl = this.controls[next];
			this.drawControls();
		}
	};

	private handleEncoderRotate = async (
		control: StreamDeckControlDefinition,
		amount: number,
	) => {
		if (control.type !== 'encoder' || !this.currentSinkControl) return;

		if (this.config.base.encoders.includes(control.index)) {
			// console.log(this.currentSinkControl, amount);

			// Volume percent adjusted by rotation amount, trapped between 0 and 100
			const reqVolume = Math.min(
				Math.max(this.currentSinkControl.volume + amount, 0),
				100,
			);
			this.pa.setSinkVolume(
				percentToVolume(reqVolume),
				this.currentSinkControl.index,
			);
		}
	};

	private updateServerInfo = async () => {
		const info = await this.pa.getServerInfo();

		const { sink } = this.matchSinkToDevice(info.defaultSink);

		if (sink === undefined) {
			console.error('Volume Control output not found');
			return;
		}

		// Get volume level(s) as needed
		// IMPORTANT: This has the possibility of race conditions if multiple
		// events are triggered
		const serverSinks = (await this.pa.getAllSinks()) as PulseAudioSink[];
		this.controls = [];
		for (let vSink of sink.sinks) {
			const matchSink = serverSinks.find((serverSink) => {
				const match = serverSink.name.match(new RegExp(vSink.match));
				return !!match;
			});

			if (!matchSink) continue;

			const control = {
				index: matchSink.index,
				volume: this.getSinkVolume(matchSink),
				name: matchSink.name,
				icon: vSink.icon,
			};

			this.controls.push(control);
			if (this.currentSinkControl?.index === control.index) {
				this.currentSinkControl = control;
			}
		}

		if (this.controls.length === 0) {
			throw Error('No matching sinks for volume control');
		}

		// Check if the current sink has changed and update things if needed
		if (this.currentSink !== info.defaultSink) {
			this.currentSink = info.defaultSink;
			this.currentSinkControl = this.controls[0];
		}

		this.drawControls();
	};

	private getSinkVolume = (sink: PulseAudioSink) => {
		return volumeToPercent(sink.volume.current[0]);
	};

	/**
	 * This is copied from @plugins/audio-toggle. Maybe reuse somehow?
	 * @param sinkName
	 * @returns
	 */
	private matchSinkToDevice = (sinkName: string) => {
		const ci = this.config.outputDevices.findIndex((check) => {
			const regex = new RegExp(check.match);
			const match = sinkName.match(regex);
			return match;
		});

		return {
			idx: ci,
			sink: ci > -1 ? this.config.outputDevices[ci] : undefined,
		};
	};

	private drawControls = async () => {
		const encoder = this.config.base.encoders[0];

		const { width, height } = this.getLcdSegmentSize(encoder);

		const paths: string[] = [];

		const bPad = 10; // Padding between controls
		const cPad = 2; // Internal padding
		const cWidth = 20; // Control Width
		const iconSize = 30;

		let x = cPad / 2;

		for (let [i, vol] of this.controls.entries()) {
			const isCurrent = vol === this.currentSinkControl;

			x += i * (cWidth + bPad);
			const y = cPad / 2;

			const volHeight = ((height - cPad - 2 * cPad) * vol.volume) / 100;
			const totalVolHeight = height - 2 * cPad;

			if (vol.icon) {
				try {
					const iconSvg = await getMdiIcon(vol.icon, {
						asSvg: true,
						size: iconSize,
						fillColor: isCurrent ? '#fff' : '#bbb',
						x,
						y,
					});

					paths.push(iconSvg);
					x += iconSize + cPad;
				} catch (e) {
					console.error(e);
				}
			}

			paths.push(`
			<g>
				<rect
					rx="${cWidth / 2}"
					height="${height - cPad}"
					width="${cWidth}"
					x="${x}"
					y="${y}"
					stroke="${isCurrent ? '#fff' : '#bbb'}"
					fill="#000"
					stroke-width="2"
				/>
				<rect
					rx="${cWidth / 2 - cPad}"
					height="${volHeight}"
					width="${cWidth - 2 * cPad}"
					x="${x + cPad}"
					y="${totalVolHeight - volHeight + cPad / 2}"
					stroke="#000"
					fill="${isCurrent ? '#fff' : '#bbb'}"
				/>
			</g>
			`);
		}

		let svg = `<svg xmlns="http://www.w3.org/2000/svg" width="${width}" height="${height}" fill="#f00">`;
		for (let path of paths) {
			svg += path;
		}
		svg += '</svg>';
		this.drawSVGLcdControl(svg, encoder);
	};
}
