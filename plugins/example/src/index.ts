import { PluginBase } from '@plugins/base';
import { ConfigSchema } from './config';

export default class PluginExample extends PluginBase<ConfigSchema> {
	public constructor(config: ConfigSchema) {
		super(config);
	}

	public init = async () => {};

	public shutdown = async () => {};
}
