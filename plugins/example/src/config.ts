import { baseConfigSchema } from '@plugins/base';
import { z } from 'shared';

export const configSchema = baseConfigSchema.merge(z.object({}));

export type ConfigSchema = z.infer<typeof configSchema>;
