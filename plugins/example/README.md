# Example

This doesn't actually do anything except serve as a super barebones starting point for creating new plugins

## Example config

```json
{
	"base": { "buttons": 3 },
	"plugins": [
		{
			"package": "@plugins/example",
			"config": {}
		}
	]
}
```
